#!/usr/bin/env python
# -*- coding: utf-8 -*-
########################################################################################################################

from sklearn.tree import DecisionTreeClassifier, export_graphviz
import subprocess
import pandas

from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

def lecture_data(file):
    data = pandas.read_csv(file, index_col=0)
    return data

def classification(data, titre_colonne, data2):
    features = list(data.columns[1:66])
    y = data[titre_colonne]
    X = data[features]

    clf = DecisionTreeClassifier()
    clf.fit(X, y)  # référence

    data_title = list(data2.columns[1:66])
    Z = data2[data_title]
    result = clf.predict(Z)
    result_model = result[0]
    return result_model







