import numpy
from statsmodels.tsa.ar_model import AR
import scipy.stats.stats as st
from math import *
import scipy
import math

x1 = 163.50 #position des capteurs envers la balance connectée
x2 = 163.50
x3 = -163.50
x4 = -163.50

y1 = -163.50
y2 = 163.50
y3 = 163.50
y4 = -163.50

#détermination des coordonnées du centre des forces selon les données des capteurs
def gcoord_computing(line):
    xG = ((float(line[1]) * x1 + float(line[2]) * x2 + float(line[3]) * x3 + float(line[4]) * x4) / float(line[5]))
    yG = ((float(line[1]) * y1 + float(line[2]) * y2 + float(line[3]) * y3 + float(line[4]) * y4) / float(line[5]))
    return xG, yG

def feature_mean(signal):
    return numpy.mean(signal)

def feature_corr(signal1, signal2):
    return  scipy.signal.correlate(signal1, signal2, mode='full')

def feature_ar_coeff(signal):
    return AR(signal).fit().params

def feature_energy(signal):
    return sum(abs(numpy.square(signal)))

def feature_freq_skewness(signal):
    return st.skew(signal)

def feature_entropy(signal):
    total_energy = feature_energy(signal)
    psd_norm = numpy.divide(numpy.square(signal), total_energy)
    entropy_tmp = 0.0

    for elem in psd_norm:
        entropy_tmp += elem * numpy.log10(elem)

    return -entropy_tmp

def Fourier(data):
    Fs = 10
    n = len(data)  # length of the signal
    Y = numpy.fft.fft(data) / n  # fft computing and normalization
    Y = Y[range((int)(n / 2))]
    k = numpy.arange(n)
    T = n / Fs
    frq = k / T  # two sides frequency range
    frq = frq[range((int)(n / 2))]  # one side frequency range
    return abs(Y), frq #on garde les composantes réelles

def ellipse_fisher(SD_ML, SD_AP, covariance):
    area_fisher = 2*math.pi*3*sqrt((SD_ML**2)*(SD_AP**2)+covariance)
    return area_fisher

def ellipse_chi(SD_AP, SD_ML):
    area_chi = math.pi * sqrt(5.991 * SD_AP**2) * sqrt(5.991 * SD_ML**2)
    return area_chi

def surface_totale(area_fisher, area_chi):
    surface = (area_fisher + area_chi)/2
    return surface

def DCT(signal):
    return scipy.fftpack.dct(signal, type=2, n=None, axis=-1, norm=None, overwrite_x=False)

def LFS(trajec, S):
    lfs = trajec/396*exp(0.0008*S)
    return lfs

def romberg_feature(SFY, SFO):
    para_romberg = 100*(SFY/SFO)
    return para_romberg
