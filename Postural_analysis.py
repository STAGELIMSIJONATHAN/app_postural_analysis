#!/usr/bin/env python
# -*- coding: utf-8 -*-
########################################################################################################################

# import des bibliothèques, libraires
import csv

import matplotlib

from preprocessing import features
from preprocessing import filtrage as fl

matplotlib.use('TkAgg')
from pylab import *


########################################################################################################################

def calcul_features(filename):
    with open(filename, "r") as mon_fichier:  # on lit notre fichier csv
        coeff_x = []
        coeff_y = []
        data = []
        data1 = []
        coordonnees_xG = []
        coordonnees_yG = []
        temps_ms = []
        pathlength_value, vitesse, surface, SD_ML, SD_AP, deplacement_x, deplacement_y, covariance, para_LFS, energie_pourcent_x1, energie_pourcent_x2, energie_pourcent_x3, energie_pourcent_y1, energie_pourcent_y2, energie_pourcent_y3, pourcent_skewness_x1, pourcent_skewness_x2, pourcent_skewness_x3, pourcent_skewness_y1, pourcent_skewness_y2, pourcent_skewness_y3, pourcent_entropy_x1, pourcent_entropy_x2, pourcent_entropy_x3, pourcent_entropy_y1, pourcent_entropy_y2, pourcent_entropy_y3, temps_s = [
            0.0 for _ in range(28)]

        dialect = csv.Sniffer().sniff(mon_fichier.read(), delimiters=',:')
        mon_fichier.seek(0)
        lecteur = csv.reader(mon_fichier, dialect)  # séparation des données

        # Je parcours le fichier texte, ainsi pour chaque ligne :
        for line in lecteur:
            xG, yG = features.gcoord_computing(line)
            coordonnees_xG.append(round(xG, 2))  # arrondi au centième et inclusion dans la liste
            coordonnees_yG.append(round(yG, 2))
            temps_ms.append(line[0])

        ########################################################################################################################

    data = coordonnees_xG
    data1 = coordonnees_yG

    ########################################################################################################################

    # Transformée de Fourier du signal de départ
    X, frq = features.Fourier(data)
    Y, frq1 = features.Fourier(data1)

    ########################################################################################################################

    # filtrage du signal des coordonnées xG en 3 bandes de fréquences
    fs = 10
    lowcut = 0.5
    highcut = 2
    cutoff = 0.5
    cutoff2 = 2

    # Filtrage xG
    # bande 0 à 0,5 Hz
    x_plage = fl.butter_lowpass_filter(data, cutoff, fs)
    # bande 0,5 à 2 Hz
    x_plage1 = fl.butter_bandpass_filter(data, lowcut, highcut, fs)
    # bande 2Hz au reste
    x_plage2 = fl.butter_highpass_filter(data, cutoff2, fs)

    # Filtrage yG
    #  bande 0 à 0,5 Hz
    y_plage = fl.butter_lowpass_filter(data1, cutoff, fs)
    # bande 0,5 à 2 Hz
    y_plage1 = fl.butter_bandpass_filter(data1, lowcut, highcut, fs)
    # bande 2Hz au reste
    y_plage2 = fl.butter_highpass_filter(data1, cutoff2, fs)

    ########################################################################################################################

    # calcul energie du signal x
    energie_x_1 = features.feature_energy(x_plage)
    energie_x_2 = features.feature_energy(x_plage1)
    energie_x_3 = features.feature_energy(x_plage2)
    energie_x = energie_x_1 + energie_x_2 + energie_x_3
    energie_pourcent_x1 = energie_x_1 / energie_x * 100
    energie_pourcent_x2 = energie_x_2 / energie_x * 100
    energie_pourcent_x3 = energie_x_3 / energie_x * 100

    # calcul energie du signal y
    energie_y_1 = features.feature_energy(y_plage)
    energie_y_2 = features.feature_energy(y_plage1)
    energie_y_3 = features.feature_energy(y_plage2)
    energie_y = energie_y_1 + energie_y_2 + energie_y_3
    energie_pourcent_y1 = energie_y_1 / energie_y * 100
    energie_pourcent_y2 = energie_y_2 / energie_y * 100
    energie_pourcent_y3 = energie_y_3 / energie_y * 100

    ########################################################################################################################

    # Calcul skewness du signal en x
    skewness_x_1 = features.feature_freq_skewness(x_plage)
    skewness_x_2 = features.feature_freq_skewness(x_plage1)
    skewness_x_3 = features.feature_freq_skewness(x_plage2)
    skewness_x = skewness_x_1 + skewness_x_2 + skewness_x_3
    pourcent_skewness_x1 = skewness_x_1 / skewness_x * 100
    pourcent_skewness_x2 = skewness_x_2 / skewness_x * 100
    pourcent_skewness_x3 = skewness_x_3 / skewness_x * 100

    # Calcul skewness du signal en y
    skewness_y_1 = features.feature_freq_skewness(y_plage)
    skewness_y_2 = features.feature_freq_skewness(y_plage1)
    skewness_y_3 = features.feature_freq_skewness(y_plage2)
    skewness_y = skewness_y_1 + skewness_y_2 + skewness_y_3
    pourcent_skewness_y1 = skewness_y_1 / skewness_y * 100
    pourcent_skewness_y2 = skewness_y_2 / skewness_y * 100
    pourcent_skewness_y3 = skewness_y_3 / skewness_y * 100

    ########################################################################################################################
    # Calcul entropy du signal en x
    entropy_x_1 = features.feature_entropy(x_plage)
    entropy_x_2 = features.feature_entropy(x_plage1)
    entropy_x_3 = features.feature_entropy(x_plage2)
    entropy_x = entropy_x_1 + entropy_x_2 + entropy_x_3
    pourcent_entropy_x1 = entropy_x_1 / entropy_x * 100
    pourcent_entropy_x2 = entropy_x_2 / entropy_x * 100
    pourcent_entropy_x3 = entropy_x_3 / entropy_x * 100

    # Calcul entropy du signal en y
    entropy_y_1 = features.feature_entropy(y_plage)
    entropy_y_2 = features.feature_entropy(y_plage1)
    entropy_y_3 = features.feature_entropy(y_plage2)
    entropy_y = entropy_y_1 + entropy_y_2 + entropy_y_3
    pourcent_entropy_y1 = entropy_y_1 / entropy_y * 100
    pourcent_entropy_y2 = entropy_y_1 / entropy_y * 100
    pourcent_entropy_y3 = entropy_y_1 / entropy_y * 100

    ########################################################################################################################
    # Calcul coeff AR
    coeff_x = features.feature_ar_coeff(data)
    coeff_y = features.feature_ar_coeff(data1)

    ########################################################################################################################
    # calcul du pathlength
    Nb_points = len(coordonnees_xG) - 1
    n = 0
    while (n < Nb_points):
        pathlength_value += sqrt(
            (coordonnees_xG[n + 1] - coordonnees_xG[n]) ** 2 + (coordonnees_yG[n + 1] - coordonnees_yG[n]) ** 2)
        n += 1
    ########################################################################################################################

    # Calcul ecart type
    tableau_x = np.array(coordonnees_xG)
    tableau_y = np.array(coordonnees_yG)
    ecart_type_x = np.std(tableau_x)
    ecart_type_y = np.std(tableau_y)
    SD_ML = round(ecart_type_x, 2)  # arrondi
    SD_AP = round(ecart_type_y, 2)

    #  Calcul des moyennes dans les positions ML et AP
    moyenne_x = features.feature_mean(coordonnees_xG)
    moyenne_y = features.feature_mean(coordonnees_yG)
    moyenne_ML = round(moyenne_x, 2)  # arrondi des moyennes
    moyenne_AP = round(moyenne_y, 2)

    ########################################################################################################################

    # Durée de l'expérience
    temps_ms = [int(i) for i in temps_ms]
    temps_s = (round((max(temps_ms) - min(temps_ms)) / 1000, 1))

    ########################################################################################################################

    # Calcul de la vitesse sur l'ensemble de l'examen
    vitesse = pathlength_value / temps_s

    ########################################################################################################################

    # Calcul surface ellipse
    #  Covariance
    for xG, yG in zip(coordonnees_xG, coordonnees_yG):
        covariance += (xG - moyenne_ML) * (yG - moyenne_AP) / Nb_points

    # 1er calcul ellipse
    area_1 = features.ellipse_fisher(SD_ML, SD_AP, covariance)
    # 2ème calcul
    area_2 = features.ellipse_chi(SD_AP, SD_ML)

    # Moyenne totale de la surface donnant donc une valeur plus précise
    surface = features.surface_totale(area_1, area_2)

    ########################################################################################################################

    # Calcul LFS
    para_LFS = features.LFS(pathlength_value, surface)

    ########################################################################################################################

    # Déplacement maximum
    deplacement_x = max(coordonnees_xG) - min(coordonnees_xG)
    deplacement_y = max(coordonnees_yG) - min(coordonnees_yG)
    mini_x = min(coordonnees_xG)
    maxi_x = max(coordonnees_xG)
    mini_y = min(coordonnees_yG)
    maxi_y = max(coordonnees_yG)
    ########################################################################################################################

    return mini_x, maxi_x, mini_y, maxi_y, coordonnees_xG, coordonnees_yG, frq, X, energie_x_1, energie_x_2, energie_x_3, energie_y_1, energie_y_2, energie_y_3, temps_s, energie_x, energie_y, surface, SD_ML, SD_AP, deplacement_x, deplacement_y, para_LFS, pathlength_value, vitesse, energie_pourcent_x1, \
           energie_pourcent_x2, energie_pourcent_x3, energie_pourcent_y1, energie_pourcent_y2, energie_pourcent_y3, pourcent_entropy_x1, \
           pourcent_entropy_x2, pourcent_entropy_x3, pourcent_entropy_y1, pourcent_entropy_y2, pourcent_entropy_y3, pourcent_skewness_x1, \
           pourcent_skewness_x2, pourcent_skewness_x3, pourcent_skewness_y1, pourcent_skewness_y2, pourcent_skewness_y3, \
           coeff_x[0], coeff_x[1], coeff_x[2], coeff_x[3], coeff_x[4], coeff_x[5], coeff_x[6], coeff_x[7], coeff_x[8], \
           coeff_x[9], coeff_x[10], coeff_x[11], coeff_x[12], coeff_x[13], coeff_x[14], coeff_x[15], coeff_x[16], \
           coeff_y[0], coeff_y[1], coeff_y[2], coeff_y[3], coeff_y[4], coeff_y[5], coeff_y[6], coeff_y[7], coeff_y[8], \
           coeff_y[9], coeff_y[10], coeff_y[11], coeff_y[12], coeff_y[13], coeff_y[14], coeff_y[15], coeff_y[16]

