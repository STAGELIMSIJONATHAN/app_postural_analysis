#!/usr/bin/env python
# -*- coding: utf-8 -*-
########################################################################################################################

# import des bibliothèques, librairies
import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter.ttk import *
import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import pandas

import modele
import Postural_analysis

list_Age = list(range(18, 100, 1))
list_Weight = list(range(40, 121, 1))
list_Height = list(range(120, 221, 1))
list_Shoe_Size = list(range(36, 46, 1))
database = "data_user.csv"


######################################################################################### Caracteristics bouton
class FunnyButton(tk.Button):
    "Bouton de fantaisie : vert virant au rouge quand on l'actionne"

    def __init__(self, boss, **Arguments):
        tk.Button.__init__(self, boss, bg="dark green", fg="white", bd=5,
                           activebackground="red", activeforeground="yellow",
                           font=('Helvetica', 12, 'bold'), **Arguments)

################################################################################## Informations user's page
class Mon_Programme(tk.Frame):
    def __init__(self):
        tk.Frame.__init__(self, bg="ivory2")
        self.master.config(bg="ivory2")
        self.master.title("PostureApp")
        self.pack(side='top', padx=10, pady=10)
        ########################################################### Creation frames
        self.frame = tk.Frame(self, bg="ivory2")
        self.frame.grid(row=15, column=6, sticky='E', padx=5, pady=5, ipadx=5, ipady=5)

        self.stepOne = tk.LabelFrame(self, text=" Veuillez entrer vos informations : \n", font=("Helvetica", 13),
                                     bg="ivory2")
        self.stepOne.grid(row=0, columnspan=11, sticky='WE',
                          padx=5, pady=5, ipadx=5, ipady=5)

        self.files = tk.LabelFrame(self, text=" Veuillez joindre les fichiers .txt générés lors des expériences : ",
                                   font=("Helvetica", 13), bg="ivory2")
        self.files.grid(row=11, columnspan=7, sticky='W',
                        padx=5, pady=5, ipadx=5, ipady=5)
        ############################################################################## Widgets

        self.txt_name = tk.Label(self.stepOne, text='Prénom : ', font=("Helvetica", 12), bg="ivory2")
        self.txt_age = tk.Label(self.stepOne, text='Age : ', font=("Helvetica", 12), bg="ivory2")
        self.txt_gender = tk.Label(self.stepOne, text='Genre : ', font=("Helvetica", 12), bg="ivory2")
        self.txt_height = tk.Label(self.stepOne, text='Taille (cm) : ', font=("Helvetica", 12), bg="ivory2")
        self.txt_weight = tk.Label(self.stepOne, text='Poids (kg): ', font=("Helvetica", 12), bg="ivory2")
        self.txt_shoe_size = tk.Label(self.stepOne, text='Pointure : ', font=("Helvetica", 12), bg="ivory2")
        self.var = tk.IntVar()
        self.R1 = tk.Radiobutton(self.stepOne, text="Homme", variable=self.var, value=0, bg="ivory2")
        self.R2 = tk.Radiobutton(self.stepOne, text="Femme", variable=self.var, value=1, bg="ivory2")
        self.txt_age = tk.Label(self.stepOne, text='Age : ', font=("Helvetica", 12), bg="ivory2")
        self.name_entry = Entry(self.stepOne, width=22)
        self.AgeSelect = tk.StringVar()
        self.WeightSelect = tk.StringVar()
        self.HeightSelect = tk.StringVar()
        self.ShoeSizeSelect = tk.StringVar()
        self.ListAge = list_Age
        self.ListWeight = list_Weight
        self.ListHeight = list_Height
        self.ListSize = list_Shoe_Size
        self.listeAge = Combobox(self.stepOne, width=20, background='white', textvariable=self.AgeSelect, \
                                 values=self.ListAge, state='readonly')
        self.listeHeight = Combobox(self.stepOne, width=20, background='white', textvariable=self.HeightSelect, \
                                    values=self.ListHeight, state='readonly')
        self.listeWeight = Combobox(self.stepOne, width=20, background='white', textvariable=self.WeightSelect, \
                                    values=self.ListWeight, state='readonly')
        self.listeShoeSize = Combobox(self.stepOne, width=20, background='white', textvariable=self.ShoeSizeSelect, \
                                      values=self.ListSize, state='readonly')

        #################################################################################### grid widgets
        self.listeAge.grid(row=4, column=1)
        self.listeHeight.grid(row=5, column=1)
        self.listeWeight.grid(row=6, column=1)
        self.listeShoeSize.grid(row=7, column=1)
        self.txt_name.grid(row=2)
        self.txt_age.grid(row=4)
        self.txt_gender.grid(row=3)
        self.txt_height.grid(row=5)
        self.txt_weight.grid(row=6)
        self.txt_shoe_size.grid(row=7)
        self.R1.grid(row=3, column=1, sticky='W')
        self.R2.grid(row=3, column=2, sticky='W')
        self.name_entry.grid(row=2, column=1, sticky='W')

        ####################################################################################################
        YO_ST_label = tk.Label(self.files, text="Sélectionner le fichier de l'expérience : ", font=("Helvetica", 12),
                               bg="ivory2")
        YO_ST_label.grid(row=0, column=0, sticky='E', padx=5, pady=2)

        self.joinFileYO_ST = Entry(self.files, text="")
        self.joinFileYO_ST.grid(row=0, column=1, columnspan=7, sticky="WE", pady=3)

        FunnyButton(self.files, text="Parcourir...", command=self.browsecsv).grid(row=0, column=8, sticky='W', padx=5,
                                                                                  pady=2)
        #################################################################################### BOUTONS
        FunnyButton(self.frame, text="Fermer", command=self.close_windows).grid(row=1, column=3)
        FunnyButton(self.frame, text="Valider", command=self.get_values).grid(row=1, column=5)
        ######################################################################################################## Fonctions

    def result_predict_modele(self,result, frame):
        if (result) == 0:
            modele = tk.Label(frame, width=40, background='green', height= 2,
                              text='SAIN', font=("Helvetica", 18), state='disabled').grid(row=4, column=5,
                                                                         sticky='NSWE')
        else :
            modele = tk.Label(frame, width=40, background='red', height=2,
                              text='NON SAIN', font=("Helvetica", 18), state='disabled').grid(row=4, column=5,
                                                                         sticky='NSWE')

    def gender_text(self, gender):
        if (gender) == 0:
            gender = 'Homme'
        else:
            gender = 'Femme'
        return gender   #Function 0 = Homme ou 1 = Femme

    def browsecsv(self):
        tk.Tk().withdraw()
        self.filename = askopenfilename(filetypes=[("Text files", "*.csv")])
        self.joinFileYO_ST.delete(0, tk.END)
        self.joinFileYO_ST.insert(0, self.filename) #Open file

    def close_windows(self):
        self.master.destroy() #close window

    def data_user(self, raw_data):
        df = pandas.DataFrame(raw_data,
                              columns=['Nom', 'Genre', 'Age', 'Taille', 'Poids', 'Pointure', 'Surface', 'SD_ML',
                                       'SD_AP',
                                       'Dépl_Max_X', 'Dépl_Max_Y', 'LFS', 'Trajectoire', 'Vitesse',
                                       'Energie_X_1',
                                       'Energie_X_2', 'Energie_X_3',
                                       'Energie_Y_1', 'Energie_Y_2', 'Energie_Y_3', 'Entropy_X_1',
                                       'Entropy_X_2',
                                       'Entropy_X_3', 'Entropy_Y_1',
                                       'Entropy_Y_2', 'Entropy_Y_3', 'Skewness_X_1', 'Skewness_X_2',
                                       'Skewness_X_3',
                                       'Skewness_Y_1',
                                       'Skewness_Y_2', 'Skewness_Y_3', 'Coeff_AR_X0', 'Coeff_AR_X1',
                                       'Coeff_AR_X2',
                                       'Coeff_AR_X3', 'Coeff_AR_X4',
                                       'Coeff_AR_X5', 'Coeff_AR_X6', 'Coeff_AR_X7', 'Coeff_AR_X8',
                                       'Coeff_AR_X9',
                                       'Coeff_AR_X10', 'Coeff_AR_X11',
                                       'Coeff_AR_X12', 'Coeff_AR_X13', 'Coeff_AR_X14', 'Coeff_AR_X15',
                                       'Coeff_AR_X16',
                                       'Coeff_AR_Y0',
                                       'Coeff_AR_Y1', 'Coeff_AR_Y2', 'Coeff_AR_Y3', 'Coeff_AR_Y4',
                                       'Coeff_AR_Y5',
                                       'Coeff_AR_Y6', 'Coeff_AR_Y7',
                                       'Coeff_AR_Y8', 'Coeff_AR_Y9', 'Coeff_AR_Y10', 'Coeff_AR_Y11',
                                       'Coeff_AR_Y12',
                                       'Coeff_AR_Y13',
                                       'Coeff_AR_Y14', 'Coeff_AR_Y15', 'Coeff_AR_Y16'])
        # écriture au format texte
        df.to_csv("data_user.csv") #Dataframe panda of user

    def get_values(self):
        nom = self.name_entry.get()
        genre = self.var.get()
        age = self.listeAge.get()
        taille = self.listeHeight.get()
        poids = self.listeWeight.get()
        pointure = self.listeShoeSize.get()
        mini_x, maxi_x, mini_y, maxi_y, coordonnees_xG, coordonnees_yG, frq, X, energie_x_1, energie_x_2, energie_x_3, energie_y_1, energie_y_2, energie_y_3, temps_s, energie_x, energie_y, surface, SD_ML, SD_AP, deplacement_x, deplacement_y, para_LFS, pathlength_value, vitesse, energie_pourcent_x1, energie_pourcent_x2, energie_pourcent_x3, energie_pourcent_y1, energie_pourcent_y2, energie_pourcent_y3, pourcent_entropy_x1, pourcent_entropy_x2, pourcent_entropy_x3, pourcent_entropy_y1, pourcent_entropy_y2, pourcent_entropy_y3, pourcent_skewness_x1, pourcent_skewness_x2, pourcent_skewness_x3, pourcent_skewness_y1, pourcent_skewness_y2, pourcent_skewness_y3, coeff_x0, coeff_x1, coeff_x2, coeff_x3, coeff_x4, coeff_x5, coeff_x6, coeff_x7, coeff_x8, coeff_x9, coeff_x10, coeff_x11, coeff_x12, coeff_x13, coeff_x14, coeff_x15, coeff_x16, coeff_y0, coeff_y1, coeff_y2, coeff_y3, coeff_y4, coeff_y5, coeff_y6, coeff_y7, coeff_y8, coeff_y9, coeff_y10, coeff_y11, coeff_y12, coeff_y13, coeff_y14, coeff_y15, coeff_y16 = Postural_analysis.calcul_features(
            self.filename)

        raw_data = {'Nom': [nom], 'Genre': [genre], 'Age': [age], 'Taille': [taille],
                    'Poids': [poids],
                    'Pointure': [pointure], 'Surface': [surface], 'SD_ML': [SD_ML],
                    'SD_AP': [SD_AP],
                    'Dépl_Max_X': [deplacement_x], 'Dépl_Max_Y': [deplacement_y], 'LFS': [para_LFS],
                    'Trajectoire': [pathlength_value],
                    'Vitesse': [vitesse], 'Energie_X_1': [energie_pourcent_x1],
                    'Energie_X_2': [energie_pourcent_x2],
                    'Energie_X_3': [energie_pourcent_x3],
                    'Energie_Y_1': [energie_pourcent_y1], 'Energie_Y_2': [energie_pourcent_y2],
                    'Energie_Y_3': [energie_pourcent_y3],
                    'Entropy_X_1': [pourcent_entropy_x1], 'Entropy_X_2': [pourcent_entropy_x2],
                    'Entropy_X_3': [pourcent_entropy_x3],
                    'Entropy_Y_1': [pourcent_entropy_y1],
                    'Entropy_Y_2': [pourcent_entropy_y2], 'Entropy_Y_3': [pourcent_entropy_y3],
                    'Skewness_X_1': [pourcent_skewness_x1],
                    'Skewness_X_2': [pourcent_skewness_x2], 'Skewness_X_3': [pourcent_skewness_x3],
                    'Skewness_Y_1': [pourcent_skewness_y1],
                    'Skewness_Y_2': [pourcent_skewness_y2], 'Skewness_Y_3': [pourcent_skewness_y3],
                    'Coeff_AR_X0': [coeff_x0],
                    'Coeff_AR_X1': [coeff_x1], 'Coeff_AR_X2': [coeff_x2], 'Coeff_AR_X3': [coeff_x3],
                    'Coeff_AR_X4': [coeff_x4],
                    'Coeff_AR_X5': [coeff_x5], 'Coeff_AR_X6': [coeff_x6], 'Coeff_AR_X7': [coeff_x7],
                    'Coeff_AR_X8': [coeff_x8], 'Coeff_AR_X9': [coeff_x9],
                    'Coeff_AR_X10': [coeff_x10],
                    'Coeff_AR_X11': [coeff_x11],
                    'Coeff_AR_X12': [coeff_x12], 'Coeff_AR_X13': [coeff_x13],
                    'Coeff_AR_X14': [coeff_x14], 'Coeff_AR_X15': [coeff_x15], 'Coeff_AR_X16': [coeff_x16],
                    'Coeff_AR_Y0': [coeff_y0],
                    'Coeff_AR_Y1': [coeff_y1], 'Coeff_AR_Y2': [coeff_y2], 'Coeff_AR_Y3': [coeff_y3],
                    'Coeff_AR_Y4': [coeff_y4],
                    'Coeff_AR_Y5': [coeff_y5], 'Coeff_AR_Y6': [coeff_y6], 'Coeff_AR_Y7': [coeff_y7],
                    'Coeff_AR_Y8': [coeff_y8], 'Coeff_AR_Y9': [coeff_y9],
                    'Coeff_AR_Y10': [coeff_y10],
                    'Coeff_AR_Y11': [coeff_y11],
                    'Coeff_AR_Y12': [coeff_y12], 'Coeff_AR_Y13': [coeff_y13],
                    'Coeff_AR_Y14': [coeff_y14], 'Coeff_AR_Y15': [coeff_y15], 'Coeff_AR_Y16': [coeff_y16]}
        self.data_user(raw_data)
        predict_result= self.model_ML()
        self.results(mini_x, maxi_x, mini_y, maxi_y, predict_result, nom, genre, age, taille, poids, pointure, pathlength_value, vitesse, surface,
                     deplacement_x, deplacement_y, para_LFS, energie_x_1, energie_x_2, energie_x_3, energie_x,
                     energie_y_1, energie_y_2, energie_y_3, energie_y, energie_pourcent_x1, energie_pourcent_x2,
                     energie_pourcent_x3, energie_pourcent_y1, energie_pourcent_y2, energie_pourcent_y3, coordonnees_xG,
                     coordonnees_yG)

    def model_ML(self):
        df = modele.lecture_data("DataCSV/data_complete_S_PS.csv")
        data_subject = modele.lecture_data("data_user.csv")
        model_result = modele.classification(df, "Probleme", data_subject)
        return model_result

    def results(self, mini_x, maxi_x, mini_y, maxi_y, predict_result, nom, genre, age, taille, poids, pointure, pathlength_value, vitesse, surface,
                deplacement_x, deplacement_y, para_LFS, energie_x_1, energie_x_2, energie_x_3, energie_x,
                energie_y_1, energie_y_2, energie_y_3, energie_y, energie_pourcent_x1, energie_pourcent_x2,
                energie_pourcent_x3, energie_pourcent_y1, energie_pourcent_y2, energie_pourcent_y3, coordonnees_xG,
                coordonnees_yG):
        childWindow = tk.Toplevel(bg="ivory2") #Open new window
        childWindow.wm_title("Résultats test postural")
        ##############################################################################################################Frames
        info_user_frame = tk.LabelFrame(childWindow, text=" Informations sujet \n", font=("Helvetica", 13),
                                        bg="ivory2")

        info_user_frame.grid(row=0, column=0, rowspan=6, columnspan=6, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)

        energy_frame = tk.LabelFrame(childWindow, text=" Energie du signal du centre de pression \n", font=("Helvetica", 13),
                                        bg="ivory2")
        energy_frame.grid(row=8, column=8, columnspan= 11, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)

        results_frame = tk.LabelFrame(childWindow, text=" Résultats posturaux \n", font=("Helvetica", 13),
                                      bg="ivory2")
        results_frame.grid(row=0, column=8, rowspan=6, columnspan= 11, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)

        butt_frame = tk.Frame(childWindow, bg="ivory2")
        butt_frame.grid(row=21, column=15, padx=5, pady=5, ipadx=5, ipady=5)

        modele_frame = tk.LabelFrame(childWindow, text=" Prédiction du modèle \n", font=("Helvetica", 13),
                                      bg="ivory2")
        modele_frame.grid(row=10, column=8, columnspan= 11, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)

        signature_frame = tk.Frame(childWindow, bg="ivory2")
        signature_frame.grid(row=23, column=18, sticky='SE')
        ###########################################################################################################Frame results
        trajectoire_txt = tk.Label(results_frame, text='Trajectoire  ', font=("Helvetica", 11),
                                   bg="ivory2").grid(row=2, column=0, sticky='W')
        surface_txt = tk.Label(results_frame, text='Surface  ', font=("Helvetica", 11),
                               bg="ivory2").grid(row=3, column=0, sticky='W')
        vitesse_txt = tk.Label(results_frame, text='Vitesse  ', font=("Helvetica", 11),
                               bg="ivory2").grid(row=2, column=5, sticky='W')
        LFS_txt = tk.Label(results_frame, text='LFS  ', font=("Helvetica", 11),
                           bg="ivory2").grid(row=3, column=5, sticky='W')
        Disp_X_txt = tk.Label(results_frame, text=" Déplacement max X ", font=("Helvetica", 11),
                              bg="ivory2").grid(row=4, column=0, sticky='W')
        Disp_Y_txt = tk.Label(results_frame, text=" Déplacement max Y ", font=("Helvetica", 11),
                              bg="ivory2").grid(row=4, column=5, sticky='W')
        results_frame.grid_columnconfigure(3, minsize=50)
        para_trajectoire = tk.Label(results_frame, width=10, background='white',
                                    text=str("%.2f" % pathlength_value) + " mm", state='disabled').grid(row=2, column=1,
                                                                                                        sticky='W')
        para_vitesse = tk.Label(results_frame, width=10, background='white',
                                text=str("%.2f" % vitesse) + " mm/s", state='disabled').grid(row=2, column=6,
                                                                                             sticky='W')
        para_surface = tk.Label(results_frame, width=10, background='white',
                                text=str("%.2f" % surface) + " mm2", state='disabled').grid(row=3, column=1,
                                                                                            sticky='W')
        LFS_value = tk.Label(results_frame, width=10, background='white',
                             text=str("%.2f" % para_LFS) + " mm2", state='disabled').grid(row=3, column=6,
                                                                                          sticky='W')
        Disp_x = tk.Label(results_frame, width=10, background='white',
                          text=str("%.2f" % deplacement_x) + " cm", state='disabled').grid(row=4, column=1,
                                                                                           sticky='W')
        Disp_y = tk.Label(results_frame, width=10, background='white',
                          text=str("%.2f" % deplacement_y) + " cm", state='disabled').grid(row=4, column=6,
                                                                                           sticky='W')
        ############################################################################################################Frame Infos
        name_txt = tk.Label(info_user_frame, text='Prénom  ', font=("Helvetica", 11),
                            bg="ivory2").grid(row=2, column=1, sticky='W')
        gender_txt = tk.Label(info_user_frame, text='Genre  ', font=("Helvetica", 11),
                              bg="ivory2").grid(row=3, column=1, sticky='W')
        age_txt = tk.Label(info_user_frame, text='Age  ', font=("Helvetica", 11),
                           bg="ivory2").grid(row=4, column=1, sticky='W')
        height_txt = tk.Label(info_user_frame, text='Taille  ', font=("Helvetica", 11),
                              bg="ivory2").grid(row=2, column=4, sticky='W')
        weight_txt = tk.Label(info_user_frame, text=" Poids ", font=("Helvetica", 11),
                              bg="ivory2").grid(row=3, column=4, sticky='W')
        shoesize_txt = tk.Label(info_user_frame, text=" Pointure ", font=("Helvetica", 11),
                                bg="ivory2").grid(row=4, column=4, sticky='W')
        info_user_frame.grid_columnconfigure(0, minsize=100)
        info_user_frame.grid_columnconfigure(3, minsize =100)
        genre_texte = self.gender_text(genre)
        para_name = tk.Label(info_user_frame, width=10, background='white',
                             text=str(nom), state='disabled').grid(row=2, column=2,
                                                                   sticky='W')
        para_gender = tk.Label(info_user_frame, width=10, background='white',
                               text=str(genre_texte), state='disabled').grid(row=3, column=2,
                                                                             sticky='W')
        para_age = tk.Label(info_user_frame, width=10, background='white',
                            text=str(age) + " ans", state='disabled').grid(row=4, column=2,
                                                                           sticky='W')
        para_height = tk.Label(info_user_frame, width=10, background='white',
                               text=str(taille) + " cm", state='disabled').grid(row=2, column=5,
                                                                                sticky='W')
        para_weight = tk.Label(info_user_frame, width=10, background='white',
                               text=str(poids) + " kg", state='disabled').grid(row=3, column=5,
                                                                               sticky='W')
        para_shoesize = tk.Label(info_user_frame, width=10, background='white',
                                 text=str(pointure), state='disabled').grid(row=4, column=5,
                                                                            sticky='W')
        ##########################################################################################################Frame energy
        nrj_X_txt = tk.Label(energy_frame, text="Energie signal X", font=("Helvetica", 10),
                             bg="ivory2").grid(row=2,
                                               sticky='N')
        nrj_Y_txt = tk.Label(energy_frame, text="Energie signal Y", font=("Helvetica", 10),
                             bg="ivory2").grid(row=4,
                                               sticky='S')
        Phase_1_txt = tk.Label(energy_frame, text="0 à 0.5 Hz", font=("Helvetica", 9),
                               bg="ivory2").grid(row=1, column=1,
                                                 sticky='N' + 'S' + 'E' + 'W')
        Phase_2_txt = tk.Label(energy_frame, text="0.5 à 2 Hz", font=("Helvetica", 9),
                               bg="ivory2").grid(row=1, column=2,
                                                 sticky='N' + 'S' + 'E' + 'W')
        Phase_3_txt = tk.Label(energy_frame, text="2 Hz et +", font=("Helvetica", 9),
                               bg="ivory2").grid(row=1, column=3,
                                                 sticky='N' + 'S' + 'E' + 'W')
        Total_txt = tk.Label(energy_frame, text="Total", font=("Helvetica", 9),
                             bg="ivory2").grid(row=1, column=4,
                                               sticky='N' + 'S' + 'E' + 'W')
        nrj_pourcent_txt = tk.Label(energy_frame, text="Energie (%)", font=("Helvetica", 8),
                                    bg="ivory2").grid(row=3,
                                                      sticky='N' + 'S' + 'E' + 'W')
        nrj_pourcent1_txt = tk.Label(energy_frame, text="Energie (%)", font=("Helvetica", 8),
                                     bg="ivory2").grid(row=5,
                                                       sticky='N' + 'S' + 'E' + 'W')

        nrj_x_1 = tk.Label(energy_frame, width=10, background='white',
                           text=str("%.2f" % energie_x_1), state='disabled').grid(row=2, column=1, sticky='N')
        nrj_x_2 = tk.Label(energy_frame, width=10, background='white',
                           text=str("%.2f" % energie_x_2), state='disabled').grid(row=2, column=2, sticky='N')
        nrj_x_3 = tk.Label(energy_frame, width=10, background='white',
                           text=str("%.2f" % energie_x_3), state='disabled').grid(row=2, column=3, sticky='N')
        nrj_x = tk.Label(energy_frame, width=10, background='white',
                         text=str("%.2f" % energie_x), state='disabled').grid(row=2, column=4, sticky='N')
        nrj_y_1 = tk.Label(energy_frame, width=10, background='white',
                           text=str("%.2f" % energie_y_1), state='disabled').grid(row=4, column=1, sticky='S')
        nrj_y_2 = tk.Label(energy_frame, width=10, background='white',
                           text=str("%.2f" % energie_y_2), state='disabled').grid(row=4, column=2, sticky='S')
        nrj_y_3 = tk.Label(energy_frame, width=10, background='white',
                           text=str("%.2f" % energie_y_3), state='disabled').grid(row=4, column=3, sticky='S')
        nrj_y = tk.Label(energy_frame, width=10, background='white',
                         text=str("%.2f" % energie_y), state='disabled').grid(row=4, column=4, sticky='S')

        energie_pourcent_x_1 = tk.Label(energy_frame, width=10, background='white',
                                        text=str("%.2f" % energie_pourcent_x1), state='disabled').grid(row=3, column=1,
                                                                                                       sticky='S')
        energie_pourcent_x_2 = tk.Label(energy_frame, width=10, background='white',
                                        text=str("%.2f" % energie_pourcent_x2), state='disabled').grid(row=3, column=2,
                                                                                                       sticky='S')
        energie_pourcent_x_3 = tk.Label(energy_frame, width=10, background='white',
                                        text=str("%.2f" % energie_pourcent_x3), state='disabled').grid(row=3, column=3,
                                                                                                       sticky='S')

        energie_pourcent_y_1 = tk.Label(energy_frame, width=10, background='white',
                                        text=str("%.2f" % energie_pourcent_y1), state='disabled').grid(row=5, column=1,
                                                                                                       sticky='S')
        energie_pourcent_y_2 = tk.Label(energy_frame, width=10, background='white',
                                        text=str("%.2f" % energie_pourcent_y2), state='disabled').grid(row=5, column=2,
                                                                                                       sticky='S')
        energie_pourcent_y_3 = tk.Label(energy_frame, width=10, background='white',
                                        text=str("%.2f" % energie_pourcent_y3), state='disabled').grid(row=5, column=3,
                                                                                                       sticky='S')

        ############################################################################################################Frame graphe
        fig = plt.Figure(figsize=(6, 4))
        plot_fig = fig.add_subplot(111)
        plot_fig.set_title('Déplacement du centre de pression')
        plot_fig.set_xlim([(float(mini_x)-30), float(maxi_x)+30])  # échelle des axes
        plot_fig.set_ylim([(float(mini_y)-30), (float(maxi_y)+30)])
        plot_fig.set_xlabel('Déplacement médiolatéral', color='g')
        plot_fig.set_ylabel('Déplacement antéropostérieur', color='r')
        graphe = FigureCanvasTkAgg(fig, master=childWindow)
        canvas = graphe.get_tk_widget()
        plot_fig.plot(coordonnees_xG, coordonnees_yG)

        # placement des graphes
        canvas.grid(row=8, column=0, rowspan=16, sticky='WE', padx=5, pady=5)
        ############################################################################################################Model ML
        self.result_predict_modele(predict_result, modele_frame)
        ############################################################################################################Button
        self.Recommandations_test(butt_frame, childWindow, predict_result)

        signature_txt = tk.Label(signature_frame, text='PosturalApp v1.0  ', font=("Helvetica", 8),
                            bg="ivory2").grid(row=0, column=0, sticky='SE')

    def Recommandations_test(self, frame1, window1, result):
        if (result) == 0:
            FunnyButton(frame1, text="Fermer", command=window1.destroy).grid(row=0, column=0)
        else :
            FunnyButton(frame1, text="Fermer", command=window1.destroy).grid(row=0, column=0)
            FunnyButton(frame1, text="Recommandations", command=self.Recommandations).grid(row=0, column=2)

    def Recommandations(self):
        childWindow2 = tk.Toplevel(bg="ivory2")  # Open new window
        childWindow2.wm_title("Recommandations")
        frame2 = tk.Frame(childWindow2, bg="ivory2")
        frame2.pack()

        confirmation = tk.Label(frame2,
                                text="INFORMATIONS : Ceci est une prédiction, cela ne veut pas dire que vous êtes atteint d'une pathologie, nous vous conseillons néanmoins de vous diriger vers des professionnels de santé pour un diagnostic approfondi  ",
                                font=("Helvetica", 12), bg="ivory2", anchor='e')

        consignes = tk.Label(frame2,
                             text="Des exercices de renforcement musculaire peuvent améliorer votre équilibre statique",
                             font=("Helvetica", 12), bg="ivory2", anchor='e')
        thanks = tk.Label(frame2,
                          text="N'hésitez pas à consulter un professionnel de santé pour toute question éventuelle, seul lui peut donner un avis vérifié sur votre équilibre",
                          font=("Helvetica", 12), bg="ivory2", anchor='e')
        confirmation.pack()
        consignes.pack()
        thanks.pack()

        FunnyButton(frame2, text=" Fermer ", command=childWindow2.destroy).pack(side='top')

'''
# Page où l'utilisateur joint les 4 fichiers txt csv
class Page_files(tk.Toplevel):
    def __init__(self, boss, **Arguments):
        tk.Toplevel.__init__(self, boss, **Arguments)
        self.resizable(width=0, height=0)  # => empêche le redimensionnement

        self.button_frame = Frame(self)
        self.button_frame.grid(row=10, column=6, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)
        self.files = LabelFrame(self, text=" Veuillez joindre les fichiers .txt générés lors des expériences : ")
        self.files.grid(row=0, columnspan=7, sticky='W', \
                        padx=5, pady=5, ipadx=5, ipady=5)

        ####################################################################################################
        YO_ST_label = Label(self.files, text="Sélectionner le fichier de l'expérience YO_ST : ")
        YO_ST_label.grid(row=0, column=0, sticky='E', padx=5, pady=2)

        joinFileYO_ST = Entry(self.files)
        joinFileYO_ST.grid(row=0, column=1, columnspan=7, sticky="WE", pady=3)

        FunnyButton(self.files, text="Parcourir...", command=self.browsecsv).grid(row=0, column=8, sticky='W', padx=5,
                                                                                  pady=2)
        ####################################################################################################
        YF_ST_label = Label(self.files, text="Sélectionner le fichier de l'expérience YF_ST : ")
        YF_ST_label.grid(row=2, column=0, sticky='E', padx=5, pady=2)

        joinFileYF_ST = Entry(self.files)
        joinFileYF_ST.grid(row=2, column=1, columnspan=7, sticky="WE", pady=3)

        FunnyButton(self.files, text="Parcourir...", command=self.browsecsv).grid(row=2, column=8, sticky='W', padx=5,
                                                                                  pady=2)
        ####################################################################################################
        YO_DT_label = Label(self.files, text="Sélectionner le fichier de l'expérience YO_DT : ")
        YO_DT_label.grid(row=4, column=0, sticky='E', padx=5, pady=2)

        joinFileYO_DT = Entry(self.files)
        joinFileYO_DT.grid(row=4, column=1, columnspan=7, sticky="WE", pady=3)

        FunnyButton(self.files, text="Parcourir...", command=self.browsecsv).grid(row=4, column=8, sticky='W', padx=5,
                                                                                  pady=2)
        ####################################################################################################
        YF_DT_label = Label(self.files, text="Sélectionner le fichier de l'expérience YF_DT : ")
        YF_DT_label.grid(row=6, column=0, sticky='E', padx=5, pady=2)

        joinFileYF_DT = Entry(self.files)
        joinFileYF_DT.grid(row=6, column=1, columnspan=7, sticky="WE", pady=3)

        FunnyButton(self.files, text="Parcourir...", command=self.browsecsv).grid(row=6, column=8, sticky='W', padx=5,
                                                                                  pady=2)
        ####################################################################################################
        FunnyButton(self.button_frame, text="Fermer", command=self.close_windows).grid(row=1, column=3)
        FunnyButton(self.button_frame, text="Valider", command=self.close_windows).grid(row=1, column=5)

    def browsecsv(self):
        tk.Tk().withdraw()
        self.filename = askopenfilename()

    def close_windows(self):
        self.master.destroy()

    def new_window(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = Page_files(self.newWindow)
'''

# Lancement de l'application
if __name__ == "__main__":  # --- Programme de test ---
    Mon_Programme().mainloop()
